const Course = require('./models/Course');
// xem cách export này
const {mutipleMongooseToObject, mongooseToObject} = require('../../util/mongoose');

class SiteController {
    // Khi có các trang không chứa các trang khác như /news, /news/:slug,... thì nên gom chung lại với nhau
    index(req, res, next) {
        // res.render('home')
        // Get data from mongodb
        Course.find({})
            .then(courses => {
                res.render('home', {
                    courses: mutipleMongooseToObject(courses)
                });
            })
            .catch(next);
    }

    search(req, res) {
        
    }
}

// Khi sử dụng new NewsController thì sẽ tạo ra 1 instance và export ra ngoài 
module.exports = new SiteController;