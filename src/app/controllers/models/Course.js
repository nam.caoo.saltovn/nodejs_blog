const mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
const Schema = mongoose.Schema;

const Course = new Schema({
    name: { type: String },
    description: { type: String },
    image: { type: String },
    videoId: { type: String },
    level: { type: String },
    slug: { type: String, slug: 'name', unique: true } // Giá trị slug tự tạo theo name. để tạo slug mà không trùng tên nhau thì thêm unique: true
  }, {
    timestamps: true, // lưu trữ thời gian createAt và UpdateAt
  });

module.exports = mongoose.model('Course', Course);