class NewsController {
    // Get trang chủ news => Method index
    // [GET] /news
    // Nội dung của phương thức này chính là nội dung của function handler nên nhận 2 tham số req, res
    index(req, res) {
        res.render('news')
    }

    // [GET] /news/:slug => :slug là parameter in url
    show(req, res) {
        res.send('NEWS DETAIL');
    }
}

// Khi sử dụng new NewsController thì sẽ tạo ra 1 instance và export ra ngoài 
module.exports = new NewsController;