const express = require('express');
const router = express.Router();
const siteController = require('../app/controllers/SiteController.js');

// Phải để router.use('/:slug', newsController.show); trước router.use('/', newsController.index); tránh trường hợp nó chạy vào trùng cái này trước
router.get('/search', siteController.search);
router.get('/', siteController.index);

module.exports = router;