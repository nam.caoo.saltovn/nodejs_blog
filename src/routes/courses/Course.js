const express = require('express');
const router = express.Router();
const CourseController = require('../../app/controllers/CourseController.js');

// Để : vì nó là thành phần động
router.get('/create', CourseController.create);
router.post('/store', CourseController.store);
router.get('/:slug', CourseController.show);

module.exports = router;