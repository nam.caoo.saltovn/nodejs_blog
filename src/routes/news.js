const express = require('express');
const router = express.Router();
const newsController = require('../app/controllers/NewsController.js');

// Phải để router.use('/:slug', newsController.show); trước router.use('/', newsController.index); tránh trường hợp nó chạy vào trùng cái này trước
// Ở cấp nhở hơn thì không dùng use mà phải dùng đúng HTTP Method.
router.get('/:slug', newsController.show);
router.get('/', newsController.index);

module.exports = router;