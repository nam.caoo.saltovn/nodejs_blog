const newsRouter = require('./news');
const siteRouter = require('./site');
const courseRouter = require('./courses/Course');

function route(app) {
    // định nghĩa route của bạn (tuyến đường)
    // Dùng use khi có bất kỳ request nào cũng chạy vào đây rồi mới vào cấp nhở hơn.
    app.use('/news', newsRouter);
    app.use('/', siteRouter);
    app.use('/courses', courseRouter);
}

module.exports = route;