const path = require('path')
const express = require('express') // nó sẽ vào node_modules để tải express về
const morgan = require('morgan')
const handlebars = require('express-handlebars'); // Handlebars
const app = express() // express là 1 function và gọi nó thực thi sẽ trả về 1 instance đại diện cho ứng dụng nodejs xây dựng nên website của bạn
const port = 3000 // Muốn run website ở cổng nào
const route = require('./routes')
const db = require('./config/db');

// Connect to DB
db.connect();

// Khi muốn truy xuất tới các file tĩnh thì làm như sau
app.use(express.static(path.join(__dirname, 'public')))

// Middleware để lấy data từ form HTML thông qua req.body mà không bị undefined
app.use(express.urlencoded({
  extended: true
}));
// Middleware để xử lý lấy data từ code Javascript lên thông qua các thư viện trong JS (XMLHttpRequest, Fetch, axios)
app.use(express.json());

// HTTP logger
app.use(morgan('combined'))

// Template engine
app.engine('hbs', handlebars({extname: '.hbs'}));

// express-handlebars
app.set('view engine', 'hbs');
// (__dirname, 'resources', 'views')) <=> __dirname/resource/views || __dirname <=> là vị trí source
app.set('views', path.join(__dirname, 'resources', 'views')); // các view mặc định sẽ vào 'resources/views'

// Must be argument app
// Route init
route(app);

app.listen(port, () => { // app được khởi tạo từ express sẽ lắng nghe cổng 3000, localhost: 127.0.0.1
  console.log(`Example app listening at http://localhost:${port}`)
});